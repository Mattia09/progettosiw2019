package it.uniroma3.siw.progetto2019.progetto2019;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Progetto2019ApplicationTests {

	@Test
	public void contextLoads() {
	}

}

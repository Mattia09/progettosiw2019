package it.uniroma3.siw.progetto2019.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto2019.model.Utente;

public interface UtenteRepository extends CrudRepository<Utente, Long>{

}

package it.uniroma3.siw.progetto2019.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="album")
public class Album {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Long id;

		private String title;

		private int  totPhoto;

		/* dato un album ho associate molte fotografie*/
		@OneToMany(mappedBy="album")
		private List<Photo> photos;
		
		@ManyToOne
		private Photographer photographer;
		
		private Long photographerIdForm;

		
		

		public void setTotPhoto(int totPhoto) {
			this.totPhoto = totPhoto;
		}

		public void setListPhotos(List<Photo> photos) {
			this.photos = photos;
		}


		public Long getId() {
			return id;
		}

		

		public int getTotPhoto() {
			return totPhoto;
		}

		public List<Photo> getPhotos() {
			return this.photos;
		}


		@Override
		public String toString() {
			return this.title;
		}

		public Photographer getPhotographer() {
			return photographer;
		}

		public void setPhotographer(Photographer photographer) {
			this.photographer = photographer;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public void setPhotos(List<Photo> photos) {
			this.photos = photos;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public Long getPhotographerIdForm() {
			return photographerIdForm;
		}

		public void setPhotographerIdForm(Long photographerIdForm) {
			this.photographerIdForm = photographerIdForm;
		}
		
}
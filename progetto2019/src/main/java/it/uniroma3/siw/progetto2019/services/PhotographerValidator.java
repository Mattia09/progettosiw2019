package it.uniroma3.siw.progetto2019.services;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.progetto2019.model.Photographer;


@Component
public class PhotographerValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> aClass) {
		return Photographer.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors error) {
		ValidationUtils.rejectIfEmptyOrWhitespace(error, "name", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(error, "surname", "required");
	}

}

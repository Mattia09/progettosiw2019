package it.uniroma3.siw.progetto2019.controller;


import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.progetto2019.model.Album;
import it.uniroma3.siw.progetto2019.model.Photographer;
import it.uniroma3.siw.progetto2019.repository.AlbumRepository;
import it.uniroma3.siw.progetto2019.services.AlbumService;
import it.uniroma3.siw.progetto2019.services.AlbumValidator;
import it.uniroma3.siw.progetto2019.services.PhotographerService;

@Controller
public class AlbumController {

	@Autowired
	private AlbumService albumService;

	@Autowired
	private AlbumValidator albumValidator;

	@Autowired
	private PhotographerService photographerService;

	@RequestMapping(value="/addAlbum")
	public String scegliFotografo(Model model) {
		model.addAttribute("album", new Album());
		return "albumForm.html";
	}

	@RequestMapping(value="/album", method = RequestMethod.POST)
	public String newAlbum(Model model,@Valid @ModelAttribute("album") Album album,BindingResult bindingResult) {
		this.albumValidator.validate(album, bindingResult);
		if(bindingResult.hasErrors()) {
			return "albumForm.html";
		}
		try {
			Photographer photographer = this.photographerService.trovaPerId(album.getPhotographerIdForm());
			album.setPhotographer(photographer);
			//Inserisco il nuovo album nel DB
			this.albumService.inserisci(album);
			model.addAttribute("albums",album);
			return "albums.html";
		}catch (NoSuchElementException e) {
			return "albumForm.html";
		}
	}
	
	@RequestMapping(value = "/album/{id}", method = RequestMethod.GET)
	public String getAlbum(@PathVariable ("id") Long id, Model model) {
		if(id !=null) {
			model.addAttribute("album",this.albumService.trovaPerId(id));
			return "album.html";
		}
		model.addAttribute("albums",this.albumService.tutti());
		return "albums.html";

	}

}

package it.uniroma3.siw.progetto2019.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.progetto2019.model.Photo;
import it.uniroma3.siw.progetto2019.repository.PhotoRepository;

@Service
public class PhotoService {
	
	@Autowired
	private PhotoRepository photoRepository;
	
	@Transactional
	public Photo inserisci(Photo p) {
		return this.photoRepository.save(p);
	}
	
	@Transactional
	public void remove(Photo p) {
		this.photoRepository.delete(p);
	}
	
	@Transactional
	public Iterable<Photo> tutti(){
		return this.photoRepository.findAll();
	}
	
	@Transactional
	public Photo trovaPerId(Long id) {
		return this.photoRepository.findById(id).get();
	}
	
	
	
}

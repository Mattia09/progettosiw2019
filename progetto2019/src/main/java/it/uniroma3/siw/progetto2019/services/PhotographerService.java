package it.uniroma3.siw.progetto2019.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.progetto2019.model.Photographer;
import it.uniroma3.siw.progetto2019.repository.PhotographerRepository;

@Service
public class PhotographerService {
	
	@Autowired
	private PhotographerRepository photographerRepository;
	
	@Transactional
	public Photographer inserisci (Photographer photographer) {
		return this.photographerRepository.save(photographer);
	}
	@Transactional
	public void rimuovi (Photographer photographer) {
		this.photographerRepository.delete(photographer);
	}
	
	@Transactional
	public Iterable<Photographer> tutti(){
		return this.photographerRepository.findAll();
	}
	@Transactional
	public Photographer trovaPerId (Long id) {
		return this.photographerRepository.findById(id).get();
	}
	
	@Transactional
	public Photographer trovaPerNome (String nome, String cognome) {
		return this.photographerRepository.findByNameAndSurname(nome, cognome);
	}

}

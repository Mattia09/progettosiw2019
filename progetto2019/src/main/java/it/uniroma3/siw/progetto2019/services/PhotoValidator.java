package it.uniroma3.siw.progetto2019.services;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.progetto2019.model.Photo;

@Component
public class PhotoValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return Photo.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "albumIdForm", "required");
	}
}

package it.uniroma3.siw.progetto2019.controller;

import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Base64;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.progetto2019.model.Album;
import it.uniroma3.siw.progetto2019.model.Photo;
import it.uniroma3.siw.progetto2019.services.AlbumService;
import it.uniroma3.siw.progetto2019.services.PhotoService;
import it.uniroma3.siw.progetto2019.services.PhotoValidator;

@Controller
public class PhotoController {

	@Autowired
	private PhotoService photoService;

	@Autowired
	private PhotoValidator photoValidator;

	@Autowired
	private AlbumService albumService;

	@RequestMapping("/addPhoto")
	public String scegliAlbum(Model model) {
		model.addAttribute("photo", new Photo());
		return "photoForm.html";
	}

	@RequestMapping(value = "/photo", method=RequestMethod.POST)
	public String newPhoto(@Valid@ModelAttribute("photo") Photo photo,
			Model model, BindingResult bindingResult) {

		this.photoValidator.validate(photo, bindingResult);
		if(bindingResult.hasErrors()) {
			return "photoForm.html";
		}
		try {
			Album album = this.albumService.trovaPerId(photo.getAlbumIdForm());
			photo.setAlbum(album);
			this.photoService.inserisci(photo);
			model.addAttribute("photos", photo);
			return "photos.html";
		}catch (NoSuchElementException e){
			return "photoForm.html";
		}
	}
	
	@RequestMapping(value = "/photo/{id}", method = RequestMethod.GET)
	public String showProductImage(@PathVariable ("id") Long id, Model model) {
		if(id !=null) {
			Photo photo = this.photoService.trovaPerId(id);
			Base64.getEncoder().encodeToString(photo.getImgPhoto());
			model.addAttribute("photo",photo);
			return "photo.html";
		}
		model.addAttribute("photos",this.albumService.tutti());
		return "photos.html";

	}
}






package it.uniroma3.siw.progetto2019.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto2019.model.Photographer;

public interface PhotographerRepository extends CrudRepository<Photographer, Long> {

	public Photographer findByNameAndSurname(String name,String surname);
	public Optional<Photographer> findById(Long id);
}
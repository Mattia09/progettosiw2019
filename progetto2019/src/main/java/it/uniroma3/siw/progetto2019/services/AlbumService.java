package it.uniroma3.siw.progetto2019.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.progetto2019.model.Album;
import it.uniroma3.siw.progetto2019.repository.AlbumRepository;

@Service
public class AlbumService {
	
	@Autowired 
	private AlbumRepository albumRepository;
	
	@Transactional
	public Album inserisci(Album album) {
		return this.albumRepository.save(album);
	}
	@Transactional
	public void remove(Album album) {
		this.albumRepository.delete(album);
	}
	
	@Transactional
	public Iterable<Album> tutti(){
		return this.albumRepository.findAll();
	}

	@Transactional
	public Album trovaPerId(Long id) {
		return this.albumRepository.findById(id).get();
	}
}

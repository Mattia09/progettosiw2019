package it.uniroma3.siw.progetto2019.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto2019.model.Photo;


public interface PhotoRepository extends CrudRepository<Photo, Long> {
}

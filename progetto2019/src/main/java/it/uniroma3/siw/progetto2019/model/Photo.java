package it.uniroma3.siw.progetto2019.model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="photo")
public class Photo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String name;

	@ManyToOne
	private Album album;

	private Long albumIdForm;

	@ManyToMany
	private List<Form> forms;
	
	@Column(nullable = false)
	@Lob
	private byte[] imgPhoto;

	//Getter&Setter
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public Album getAlbum() {
		return album;
	}

	public List<Form> getForms() {
		return this.forms;
	}

	public void setForms(List<Form> forms) {
		this.forms = forms;
	}

	public Long getAlbumIdForm() {
		return this.albumIdForm;
	}

	public void setAlbumIdForm(Long albumIdForm) {
		this.albumIdForm = albumIdForm;
	}

	public byte[] getImgPhoto() {
		return imgPhoto;
	}

	public void setImgPhoto(byte[] imgPhoto) {
		this.imgPhoto = imgPhoto;
	}

}
package it.uniroma3.siw.progetto2019;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Progetto2019Application {

	public static void main(String[] args) {
		SpringApplication.run(Progetto2019Application.class, args);
	}

}

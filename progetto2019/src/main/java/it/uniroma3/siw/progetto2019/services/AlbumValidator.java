package it.uniroma3.siw.progetto2019.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.progetto2019.model.Album;
import it.uniroma3.siw.progetto2019.model.Photographer;

@Component
public class AlbumValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> aClass) {
		return Album.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors error) {
		ValidationUtils.rejectIfEmptyOrWhitespace(error, "title", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(error, "photographerIdForm", "required");
	}
	

}

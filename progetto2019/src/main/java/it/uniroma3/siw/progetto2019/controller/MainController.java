package it.uniroma3.siw.progetto2019.controller;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.progetto2019.model.Photo;
import it.uniroma3.siw.progetto2019.services.PhotoService;

@Controller
public class MainController {
	
	@Autowired
	private PhotoService photoService;
	
	@RequestMapping(value= "/welcome")
	public String welcome(Model model) {
		return "welcome.html";
	}
	
    @RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
    public String admin(Model model) {
        UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String role = details.getAuthorities().iterator().next().getAuthority();    // get first authority
        model.addAttribute("username", details.getUsername());
        model.addAttribute("role", role);

        return "admin.html";
    }
    
    @RequestMapping(value = {"/gallery"}, method = RequestMethod.GET)
    public String gallery(Model model) {
    	Iterable<Photo> photos = this.photoService.tutti();
    	for(Photo photo :photos) {
    		Base64.getEncoder().encodeToString(photo.getImgPhoto());
    	}
    	model.addAttribute("photos",photos);
    	return "gallery.html";
    }
}

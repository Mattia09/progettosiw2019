package it.uniroma3.siw.progetto2019.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto2019.model.Album;

public interface AlbumRepository extends CrudRepository<Album, Long> {

}

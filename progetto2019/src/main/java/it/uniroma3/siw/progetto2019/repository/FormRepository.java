package it.uniroma3.siw.progetto2019.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto2019.model.Form;

public interface FormRepository extends CrudRepository<Form, Long>{

}

package it.uniroma3.siw.progetto2019.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.progetto2019.model.Photographer;
import it.uniroma3.siw.progetto2019.services.PhotographerService;
import it.uniroma3.siw.progetto2019.services.PhotographerValidator;


@Controller
public class PhotographerController {

	@Autowired
	private PhotographerService photographerService;
	@Autowired
	private PhotographerValidator photographerValidator;


	@RequestMapping("/addPhotographer")
	public String addPhotographer(Model model) {
		model.addAttribute("photographer", new Photographer());
		return "photographerForm.html";
	}

	@RequestMapping(value = "/photographer", method = RequestMethod.POST)
	public String newPhotographer(@Valid @ModelAttribute("photographer") Photographer photographer,
			Model model, BindingResult bindingResult) {
		this.photographerValidator.validate(photographer, bindingResult);
		if(!bindingResult.hasErrors()) {
			this.photographerService.inserisci(photographer);
			model.addAttribute("photographers", this.photographerService.tutti());
			return "photographers.html";
		}else {
			return "photographerForm.html";
		}
	}

	@RequestMapping(value = "/photographer/{id}", method = RequestMethod.GET)
	public String getPhotographer(@PathVariable ("id") Long id, Model model) {
		if(id !=null) {
			model.addAttribute("photographer",this.photographerService.trovaPerId(id));
			return "photographer.html";
		}
		model.addAttribute("photographers",this.photographerService.tutti());
		return "photographers.html";

	}

}

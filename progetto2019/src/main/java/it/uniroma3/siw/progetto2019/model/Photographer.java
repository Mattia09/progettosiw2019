package it.uniroma3.siw.progetto2019.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "photographer")
public class Photographer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String name;
	private String surname;
	
	
	@OneToMany(mappedBy="photographer")
	private List<Album> album;
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public List<Album> getAlbum() {
		return this.album;
	}
	
	public void setAlbum(Album album) {
		this.album.add(album);
	}

	@Override
	public String toString() {
		return this.name + " " + this.surname;
	}
	
	
	
	
}

